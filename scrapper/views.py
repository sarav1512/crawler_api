from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.http.response import HttpResponse
from urllib2 import urlparse
from helper import ParserHelper
import json

@method_decorator(csrf_exempt,name='dispatch')
class PageScrapper(View):
    def post(self,request):
        print request.body
        data    = json.loads(request.body)
        url     = data.get("url")
        depth   = int(data.get("depth",1))
        host    = urlparse.urlparse(url).hostname
        response = ParserHelper().crawl(url,depth,host)
        print response
        return HttpResponse(json.dumps(response),content_type="application/json")