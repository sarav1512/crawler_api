import requests
from parsel import Selector
import traceback

class ParserHelper():
    def __init__(self):
        self.links = []
    def crawl(self,url,depth,host):
        try:
            results = []
            urls    = url if isinstance(url,list) else [url]
            self.links.extend(urls)
            while(depth):
                depth = depth - 1
                for link in urls:
                    crawl       = {"parent":link}
                    response    =   requests.get(link)
                    content     =   Selector(response.text)
                    if(depth == 0):
                        images          =   set([image for image in content.xpath('//img/@src').getall()])
                        crawl["child"]  =   list(images)
                    else:
                        hrefs           =   [ xpath for xpath in set(content.xpath('//a/@href').getall()) if host in xpath and xpath not in self.links]
                        crawl["child"]  =   self.crawl(hrefs,depth,host)
                    if len(crawl["child"]) > 0:
                        results.append(crawl)
            return results
        except Exception, e:
            print traceback.print_exc()
            print "Error in parsing url" + str(e)
            return []